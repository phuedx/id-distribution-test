'use strict';

// https://github.com/wikimedia/mediawiki/blob/master/resources/src/mediawiki.user.js#L76-L137
function generateID() {
	const crypto = window.crypto;
	let rnds, i;
	try {
		rnds = new Uint16Array( 5 );
		crypto.getRandomValues( rnds );
	} catch ( e ) {
		rnds = new Array( 5 );
		for ( i = 0; i < 5; i++ ) {
			rnds[ i ] = Math.floor( Math.random() * 0x10000 );
		}
	}
	return ( rnds[ 0 ] + 0x10000 ).toString( 16 ).slice( 1 ) +
		( rnds[ 1 ] + 0x10000 ).toString( 16 ).slice( 1 ) +
		( rnds[ 2 ] + 0x10000 ).toString( 16 ).slice( 1 ) +
		( rnds[ 3 ] + 0x10000 ).toString( 16 ).slice( 1 ) +
		( rnds[ 4 ] + 0x10000 ).toString( 16 ).slice( 1 );
}

// https://gitlab.wikimedia.org/repos/data-engineering/metrics-platform/-/blob/main/js/src/SamplingController.js?ref_type=heads#L3
const UINT32_MAX = 4294967295; // (2^32) - 1

// https://gitlab.wikimedia.org/repos/data-engineering/metrics-platform/-/blob/main/js/src/SamplingController.js?ref_type=heads#L48
function normalizeID( id ) {
	return parseInt( id.slice( 0, 8 ), 16 ) / UINT32_MAX;
}

QUnit.test( 'IDs are uniformly distributed', ( assert ) => {
	const sessionIDs = new Array( 50000 );

	for ( let i = 0; i < sessionIDs.length; ++i ) {
		sessionIDs[ i ] = normalizeID( generateID() );
	}

	const result = kstest( sessionIDs, 'uniform', 0.0, 1.0, {

		// Per Mikhail Popov:
		//
		// * The significance level ⍺ might be too high at 5%. When the token-generation code
		//   behaves like we want, H0 is true so the test will incorrectly reject H0 with
		//   probability ⍺. We may want to set it even lower, perhaps at 0.0001 because we want it
		//   to be very unlikely to have a false positive.
		// * Fortunately the number of samples appears to be large enough that the power approaches
		//   1, so we can pretty confident that when the token-generation code misbehaves and
		//   generates bad tokens (H0 is false), we will correctly reject H0.
		alpha: 0.0001
	} );

	assert.strictEqual(
		result.rejected,
		false,
		'IDs are not uniformly distributed.'
	);
} );
