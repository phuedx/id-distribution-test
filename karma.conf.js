'use strict';

const fs = require( 'fs' );

const BrowserStackCustomLauncherLookup = require( './src/BrowserStackCustomLauncherLookup' );

module.exports = function ( config ) {
	const lookup = BrowserStackCustomLauncherLookup.newInstance( __dirname + '/browsers.json' );
	const customLaunchers = lookup.getAll();

	const browsersLines =
		fs.readFileSync( __dirname + '/all_sites_by_os_and_browser_percent.tsv', 'utf-8' )
			.split( '\r\n' );

	const browsers = Object.keys( lookup.getAllByBrowsersLines( browsersLines ) );

	config.set( {
		basePath: '',
		hostname: 'bs-local.com',
		frameworks: [ 'qunit' ],
		plugins: [ 'karma-qunit', 'karma-browserstack-launcher' ],
		files: [

			// https://www.npmjs.com/package/@stdlib/stdlib#umd
			'https://cdn.jsdelivr.net/gh/stdlib-js/stats-kstest@umd/browser.js',

			'main.js'
		],
		reporters: [ 'progress', 'BrowserStack' ],
		port: 9876,
		colors: true,
		logLevel: config.LOG_INFO,
		browserStack: {
			username: process.env.BROWSERSTACK_USERNAME,
			accessKey: process.env.BROWSERSTACK_ACCESS_KEY
		},
		customLaunchers,
		browsers,
		singleRun: true,
		captureTimeout: 180000
	} );
};
