'use strict';

const IOS_BROWSER_MAP = new Map( [
	[ 'Chrome Mobile', 'chromium' ],
	[ 'Mobile Safari', 'iphone' ]
] );

const MAC_OS_X_VERSION_MAP = new Map( [
	[ '10', 'Catalina' ]
] );

function customLauncherMatchesFilter( customLauncher, filter ) {
	const entries = Object.entries( filter );

	for ( let i = 0; i < entries.length; ++i ) {
		const [ key, value ] = entries[ i ];

		if ( !customLauncher[ key ] || customLauncher[ key ] !== value ) {
			return false;
		}
	}

	return true;
}

class BrowserStackCustomLauncherLookup {
	constructor() {
		this.capabilities = new Map();
		this.index = new Map();
	}

	add( capability ) {
		capability.base = 'BrowserStack';

		const [ primaryKey, secondaryKey ] = this.getKeys( capability );

		this.capabilities.set( primaryKey, capability );

		if ( !secondaryKey ) {
			return;
		}

		if ( !this.index.has( secondaryKey ) ) {
			this.index.set( secondaryKey, new Set() );
		}

		this.index.get( secondaryKey ).add( primaryKey );
	}

	getAll() {
		return Object.fromEntries( this.capabilities.entries() );
	}

	getByBrowserslistSpec( browserslistSpec ) {
		const parts = browserslistSpec.split( ' ' );
		const browser = parts[ 0 ];

		// e.g. 16.6-16.7 -> 16.6
		let browserVersion = parts[ 1 ].split( '-' )[ 0 ];

		if ( browser === 'ios_saf' ) {
			// e.g. 16.6 -> 16
			browserVersion = browserVersion.split( '.' )[ 0 ];
		}

		const secondaryKey = [ browser, browserVersion ].join( ' ' );
		const primaryKeys =
			this.index.has( secondaryKey ) ?
				Array.from( this.index.get( secondaryKey ) ) :
				[];

		return Object.fromEntries(
			primaryKeys.map( ( entryKey ) => [ entryKey, this.capabilities.get( entryKey ) ] )
		);
	}

	getAllByBrowserslistSpec( browserslistSpecs ) {
		return browserslistSpecs.reduce(
			( result, browserslistSpec ) => Object.assign( result, this.get( browserslistSpec ) ),
			{}
		);
	}

	getByBrowsersLine( browsersLine ) {
		const parts = browsersLine.split( '\t' );
		const [
			/* _1 */,
			os,
			osVersion,
			browser,
			browserVersion
			/* _5 */
		] = parts;
		const filters = [];

		/* eslint-disable camelcase */
		if ( os === 'iOS' && IOS_BROWSER_MAP.has( browser ) ) {
			filters.push( {
				os: 'ios',
				os_version: osVersion,
				browser: IOS_BROWSER_MAP.get( browser )
			} );
		} else if ( os === 'Windows' ) {
			filters.push( {
				os,
				os_version: osVersion,
				browser: browser.toLowerCase(),
				browser_version: `${ browserVersion }.0`
			} );
		} else if ( os === 'Mac OS X' && MAC_OS_X_VERSION_MAP.has( osVersion ) ) {
			filters.push( {
				os: 'OS X',
				os_version: MAC_OS_X_VERSION_MAP.get( osVersion ),
				browser: browser.toLowerCase(),
				browser_version: `${ browserVersion }.0`
			} );
		}
		/* eslint-enable camelcase */

		return Object.fromEntries(
			Array.from( this.capabilities.entries() )
				.filter( ( [ /* _1 */, customLauncher ] ) => {
					for ( let i = 0; i < filters.length; ++i ) {
						if ( customLauncherMatchesFilter( customLauncher, filters[ i ] ) ) {
							return true;
						}
					}

					return false;
				} )
		);
	}

	getAllByBrowsersLines( browsersLines ) {
		return browsersLines.reduce(
			( result, browsersLine ) =>
				Object.assign( result, this.getByBrowsersLine( browsersLine ) ),
			{}
		);
	}

	getKeys( capability ) {
		const {
			os,
			os_version: osVersion,
			browser
		} = capability;
		let browserVersion = capability.browser_version;

		if ( os === 'ios' || os === 'android' ) {
			const { device } = capability;
			const result = [
				// Fully-qualified key
				[ device, browser, os, osVersion ].join( ' ' ).replace( /\W/g, '_' ).toLowerCase()
			];

			if ( browser === 'iphone' ) {
				// Browserslist-like key
				result.push( [ 'ios_saf', osVersion ].join( ' ' ) );
			}

			return result;
		}

		browserVersion = capability.browser_version;

		// e.g. 122.0 -> 122, 3.6 -> 3.6
		if ( browserVersion.endsWith( '.0' ) ) {
			browserVersion = browserVersion.slice( 0, Math.max( 0, browserVersion.indexOf( '.' ) ) );
		}

		return [
			// Fully-qualified key
			[ browser, browserVersion, os, osVersion ].join( ' ' ).replace( /\W/g, '_' ).toLowerCase(),

			// Browserslist-like key
			[ browser, browserVersion ].join( ' ' )
		];
	}

	static newInstance( browsersJsonPath ) {
		const result = new BrowserStackCustomLauncherLookup();

		// eslint-disable-next-line security/detect-non-literal-require
		require( browsersJsonPath ).forEach( ( capability ) => result.add( capability ) );

		return result;
	}
}

module.exports = BrowserStackCustomLauncherLookup;
